const express = require('express')
import User from '../models/userModel';
const router = express.Router();
import bodyParser from 'body-parser';

router.route('/')
    .get((req, res) => {
        User.find({}, (err, users) => {
            res.json(users)
        })
    })
    .post((req, res) => {
        let user = new User(req.body);
        user.save();
        res.status(201).send(user) 
    })
router.route('/:userId')
    .get((req, res) => {
        User.findById(req.params.userId, (err, users) => {
            res.json(users)
        })
    })
    .put((req,res) => {
        User.findById(req.params.userId, (err, user) => {
            user.password = req.body.password;
            user.email = req.body.email;
            user.save()
            res.json(user)
        }) 
    })
    .delete((req,res)=>{
        User.findById(req.params.userId, (err, user) => {
            user.remove(err => {
                if(err){
                    res.status(500).send(err)
                }
                else{
                    res.status(204).send('removed')
                }
            })
        })
    })//delete
router.route('/email/:email')
    .get((req,res) => {
        User.findOne({'email': req.params.email}, (err, user) => {
            if(err) {
                res.status(500).send(err)
            } else {
                res.json(user)
            }
            
        })
    })
export default router;