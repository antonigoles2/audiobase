const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();
const express = require('express');
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import userRoutes from './routes/userRoutes';
const db = mongoose.connect('mongodb://foedev:antek2203@ds119445.mlab.com:19445/musicbase');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/api/Users', userRoutes);

app.use(express.static('public'))

router.get('/',function(req,res){
    res.sendFile(path.join(__dirname+'/home.html'));
})
router.get('/home',function(req,res){
    res.sendFile(path.join(__dirname+'/home.html'));
})
router.get('/register',function(req,res){
    res.sendFile(path.join(__dirname+'/register.html'));
})
router.get('/login',function(req,res){
    res.sendFile(path.join(__dirname+'/login.html'));
})
  
  app.use('/', router);
  app.listen(8080);
  
  console.log('Running at Port 8080');
